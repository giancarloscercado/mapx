# MAPX

## Crea tus listas de geolocalización con MAPX


> Creado por [Giancarlos Cercado](http://giancarloscercado.com).


## Lenguajes
* Php
* Javascript

## Tecnologías
* Laravel 5.3.1
* Vue JS 2.1
* Firebase 3 (Database & Auth)
* Google Maps Api
* Gulp + webpack


## Instrucciones
* run `npm install` ó `yarn`
* run `composer install`
* run `npm run prod` , para generar el js principal
* run `php artisan serve` ó abre la ruta desde el navegador apuntando a apache o nginx (depende de donde este alojado el proyecto)
* Listo!


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
