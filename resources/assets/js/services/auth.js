import { admin } from '../firebaseDB';
import VueRouter from 'vue-router';

const router = new VueRouter({
    mode: 'history'
});

export default {

    authenticated: false,

    isLoading: false,

    hasError: false,

    login (credentials, urlRedirect, cb){

        this.isLoading = true;
        const { email, pass } = credentials;

        admin.signInWithEmailAndPassword(email, pass).then(resp => {

            localStorage.setItem('user', JSON.stringify(resp));
            this.authenticated = true;

            if(urlRedirect){
                router.push({name: 'userhome'});
            }

            this.isLoading = false;
            this.hasError = false;
            console.log('%cLoged!','color:green');
            cb(false, false, resp);//isLoading, hasError

        }, err => {
            console.log('%cSe presento un error en la autenticacion, credenciales incorrectas', 'color:red,  font-weith:25px;');
            console.log(err);
            this.isLoading = false;
            this.hasError = true;
            localStorage.removeItem('user');
            cb(false, true, null);//isLoading, hasError
        });
    },

    logout (){
        admin.signOut();
        localStorage.removeItem('user');
        this.authenticated = false;
        this.isLoading = false;
        this.hasError = false;
        router.push({name: 'login-page'});
        console.log('logout!');
    }
}
