import Firebase from 'firebase';
import config from './config/firebase';

const app = Firebase.initializeApp(config);
const adminAuth = app.auth();
const db = app.database();

module.exports = {
    admin: adminAuth,
    db: db
};

