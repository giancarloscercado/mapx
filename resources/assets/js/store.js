import Vue from 'vue';
import Vuex from 'vuex';

import userStore from './components/user/userStore';
import geoStore from './components/geo/geoStore';

Vue.use(Vuex);
const debug = process.env.NODE_ENV !== 'production';

//console.log(`STRICT MODE = ${debug}`);

export default new Vuex.Store({
    modules: { userStore, geoStore },
    strict: false //debug
});
