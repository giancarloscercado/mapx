// INIT STATE
const state = {
    georeferencia: null
};

//MUTATIONS PROCES
const mutations = {
    SET_GEOREFERENCIA (state, geoObj){
        state.georeferencia = geoObj;
    },
    CLEAR_GEOREFERENCIA (state){
        state.georeferencia = null;
    }
};

//ACTIONS TO DISPATCH
const actions = {
    setGeoreferencia: ({commit}, userObj) => {
        commit('SET_GEOREFERENCIA', userObj);
    },
    clearGeoreferencia: ({commit}) => {
        commit('CLEAR_GEOREFERENCIA');
    }
};

export default {
    state, mutations, actions
}
