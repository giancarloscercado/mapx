// INIT STATE
const state = {
    authUser: null,
    isLoading: false,
    hasError: false
};

//MUTATIONS PROCES
const mutations = {
    SET_AUTH_USER (state, userObj){
        state.authUser = userObj;
    },
    CLEAR_AUTH_USER (state){
        state.authUser = null;
    },
    SET_LOADING_REQUEST_ON (state){
        state.isLoading = true;
    },
    SET_LOADING_REQUEST_OFF (state){
        state.isLoading = false;
    },
    SET_HAS_ERROR (state){
        state.hasError = true;
    },
    SET_HAS_NO_ERROR (state){
        state.hasError = false;
    }
};

//ACTIONS TO DISPATCH
const actions = {
    setAuthUser: ({commit}, userObj) => {
        commit('SET_AUTH_USER', userObj);
    },
    clearAuthUser: ({commit}) => {
        commit('CLEAR_AUTH_USER');
    },
    setLoadingRequestOn: ({commit}) => {
        commit('SET_LOADING_REQUEST_ON');
    },
    setLoadingRequestOff: ({commit}) => {
        commit('SET_LOADING_REQUEST_OFF');
    },
    setHasError: ({commit}) => {
        commit('SET_HAS_ERROR');
    },
    setHasNoError: ({commit}) => {
        commit('SET_HAS_NO_ERROR');
    }
};

export default {
    state, mutations, actions
}
