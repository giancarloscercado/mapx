export function slugify (text){
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Reemplaza con - los espacios
        .replace(/[^\w\-]+/g, '')       // Remueve los no caracteres
        .replace(/\-\-+/g, '-')         // Reemplaza los multiples - con simple
        .replace(/^-+/, '')             // Trim - al inicio
        .replace(/-+$/, '');            // Trim - al fin
}