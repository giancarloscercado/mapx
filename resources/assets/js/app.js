/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

//vue
import Vue from 'vue';
import VueRouter from 'vue-router';

//firebase for vue
import VueFire from 'vuefire';

//use on instance
Vue.use(VueFire);
Vue.use(VueRouter);

//maps

//routes
import routes from './routes'

const router = new VueRouter({
    routes: routes,
    mode: 'history',
    linkActiveClass: 'is-active'
});

//vuex store
import store from './store';

//segurity implementation by meta prop
router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        
        const authUser = JSON.parse(window.localStorage.getItem('user'));
        if (authUser) {
            console.log('%cSi esta autenticado!', 'color:lightblue');
            next();
        }else{
            next({name: 'login-page'});
        }
    }

    next();
});

//layout component
import Layout from './layout/index.vue';

new Vue({
    router,
    store,
    //template: `<router-view></router-view>`,
    components: {Layout},
    template: `<layout></layout>`,
    name: 'application'
}).$mount('#app');
