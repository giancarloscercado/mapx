//templates or components

import Home from './pages/home.vue';
import Register from './pages/register.vue';
import Account from './pages/account.vue';
import Login from './pages/login.vue';
import HomeLoggedUser from './pages/home-logged-user.vue';
import NotFound from './pages/404.vue';
import ListaGeo from './pages/list-goe.vue';

const routes = [
    {
        path: '/',
        component: Home,
        name: 'index'
    },
    {
        path: '/login',
        component: Login,
        name: 'login-page'
    },
    {
        path: '/register',
        component: Register,
        name: 'register-page'
    },
    {
        path: '/register/:email',
        component: Register,
        name: 'register-page-with-email'
    },
    {
        path: '/home',
        component: HomeLoggedUser,
        name: 'userhome',
        meta: { requiresAuth: true }
    },
    {
        path: '/account',
        component: Account,
        name: 'account-page',
        meta: { requiresAuth: true}
    },
    {
        path: '/lista/:slug',
        component: ListaGeo,
        name: 'lista-sluggable',
        props: {slug: true}
    },
    {
        path: '/*',
        component: NotFound,
        name: 'page-not-found'
    }
];

export default routes;
