<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mapx</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.1/css/bulma.min.css" />
        <link rel="stylesheet" href="{!! secure_asset('css/mapx.css') !!}" />

        <!--
        <link rel="stylesheet" href="https://developers.google.com/maps/documentation/javascript/demos/demos.css" />
        -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.css">

    </head>
    <body>
        <!-- aqui la magia de vue :)-->
        <div id="app"></div>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvDb09NgwO5dwJuWHsD20fI9764LvJXPg"></script>

        <script src="{!! secure_asset('js/app.js') !!}"></script>

        <script type="application/javascript">
            (function() {
                var burger = document.querySelector('.nav-toggle');
                var menu = document.querySelector('.nav-menu');
                burger.addEventListener('click', function() {
                    burger.classList.toggle('is-active');
                    menu.classList.toggle('is-active');
                });
            })();
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.0.3/jquery-confirm.min.js" async></script>
    </body>
</html>
